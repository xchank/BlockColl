#pragma once


const short LEN_MD_SHA256 = 32;

void char2hex(unsigned char c, char *h);
void miner(char (&prev_hash)[LEN_MD_SHA256], int difficulty);
