#include <iostream>
#include <iomanip>
#include <string>
#include <cstdint>
#include <openssl/evp.h>
#include "utilities.h"
using std::string;

void char2hex(unsigned char c, char *h)
{
  char func[17] = "0123456789ABCDEF";
  char hex[2] = {};
  h[0] = func[(static_cast<unsigned short>(c))/16];
  h[1] = func[(static_cast<unsigned short>(c))%16];
  return;
}

uint32_t miner(char (&prev_hash)[LEN_MD_SHA256], int difficulty)
{
  uint32_t nonce = 0;
  unsigned char message_digest[LEN_MD_SHA256];
  string guess;
  EVP_MD_CTX *mdctx = EVP_MD_CTX_create();

  while (nonce < UINT32_MAX)
  {
    guess = string(prev_hash) + std::to_string(nonce);
    nonce++;
    if (1 == EVP_DigestInit_ex(mdctx, EVP_sha256(), NULL))
    {
      EVP_DigestUpdate(mdctx, guess.data(), guess.size());
      EVP_DigestFinal_ex(mdctx, message_digest, NULL);
      // if digest matches the difficulty, return nonce
    }
    else
    {//init failed
    }
  }

  EVP_MD_CTX_destroy(mdctx);
  EVP_cleanup();
  return;
}

